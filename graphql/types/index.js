const { mergeTypes } = require("merge-graphql-schemas");

const Module = require("./Module");
const Grant = require('./Grant');
const Role = require("./Roles");
const Resource = require("./Resource");
const Action = require("./Action");
const Git = require("./Git");

const typeDefs = [Grant, Module, Role, Resource, Action, Git];

module.exports = mergeTypes(typeDefs, {
    all: true
});