module.exports = 
`   
    type Query { 
        grants: [Grant]
    }

    type Mutation {
        updateGrant(data: UpdateGrantInput): Grant
    }

    type Grant {
        role: String,
        resource: String,
        action: String,
        attributes: String
    }

    input UpdateGrantInput {
        role: String,
        resource: String,
        action: String,
        attributes: String
    }
`;
