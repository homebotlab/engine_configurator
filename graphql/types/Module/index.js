module.exports = `
    type Query { 
        modules: [Module]
        module(_id: String): Module!
    }

    type Mutation {
        updateModule(_id: String, data: UpdateModuleInput): Module
        copyModule(_id: String!, newId: String!): Boolean
        loadModule(url: String!, username: String, password: String): Boolean
    }

    type Module { 
        _id: String
        title: String
        src: String
        lastCommit: String
        description: String
        isCopied: Boolean
        grants: [Grant]
    }

    input UpdateModuleInput {
        title: String,
        description: String,
        grants: [UpdateGrantInput]
    }
`;
