const { mergeResolvers } = require("merge-graphql-schemas");

const Grant = require('./Grant'); 
const Module = require('./Module');
const Role = require('./Roles');
const Resource = require('./Resource');
const Action = require('./Action');
const Git = require('./Git');

const resolvers = [Grant, Module, Role, Resource, Action, Git];

module.exports = mergeResolvers(resolvers);