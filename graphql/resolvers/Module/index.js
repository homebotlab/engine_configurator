const { join } = require('path');
const moduleInstaller = require("../../../model/Installer");
const configurator = require(`${process.env.HELPERS_PATH}/modules`);
const {
	NotFoundError,
	BadRequestError
} = require(`${process.env.HELPERS_PATH}/graphqlError`);

module.exports = {
	Query: {
		modules: () => {
			const modules = configurator.getModules();
			return modules;
		},
		module: (parent, {
			_id
		}) => {
			const modules = configurator.getModules();
			const currModule = modules.find(item => item._id == _id);
			if (!currModule) return NotFoundError;

			return {
				...currModule,
				isCopied: !!currModule.replicationClass
			}
		}
	},

	Mutation: {
		updateModule: async (parent, { _id, data }) => {
			const updatedModule = await configurator.updateModule(_id, data);
			return updatedModule;
		},

		copyModule: async (parent, { _id, newId }) => {
			const currModule = configurator.getModule(_id);
			if (!newId) return BadRequestError;
			
			if (currModule) {
				if (currModule.data.replicationClass) {
					const replPath = join(
						process.cwd(),
						currModule.meta.src,
						"../",
						currModule.data.replicationClass
					  );
					const Replication = require(replPath);
					
					// coping module
					const replication = new Replication();
					await replication.copy(newId);

					// loading info about new module
					configurator.loadModules();
				}
			} else NotFoundError;
		},
		
		loadModule: async (parent, { url, username, password }) => {
			// username && password needed for private repo
			await moduleInstaller.install(url, username, password);
		}
	}
}