const Git = require('nodegit');
const { join } = require('path');

module.exports = {
  Query: {
    lastCommit: async () => {
        const rootGitPath = join(__dirname, '../../../../../../.git');
        const gitRepo = await Git.Repository.open(rootGitPath);
        const headCommit = await gitRepo.getHeadCommit();
        const commitHash = headCommit.toString().slice(0, 7);

        return commitHash;
    }
  }
}