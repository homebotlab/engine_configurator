const configurator = require(`${process.env.HELPERS_PATH}/modules`);

module.exports = {
  Query: {
    resources: () => {
      const resources = configurator.getResourceList();
      return resources;
    }
  }
}