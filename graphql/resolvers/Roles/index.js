const configurator = require(`${process.env.HELPERS_PATH}/modules`);

module.exports = {
  Query: {
    roles: () => {
      const roles = configurator.getRoleList();
      return roles;
    }
  }
}