module.exports = {
  Query: {
    actions: () => {
      return [
          "create:own",
          "create:any",
          "read:own",
          "read:any",
          "update:own",
          "update:any",
          "delete:own",
          "delete:any",
      ]
    }
  }
}