const Git = require("nodegit");
const path = require("path");

async function install (gitUrl, gitUserName, gitPassword) {
    const moduleName = gitUrl.split("/").slice(-1)[0].slice(0, -4); 
    const cloneOptions = {
        fetchOpts: {
            callbacks: {
                credentials: () => Git.Cred.userpassPlaintextNew(gitUserName, gitPassword)
            }
        }
    }

    // TODO: rewrite if exist
    return Git.Clone(gitUrl, path.resolve(__dirname, "../../../../app/modules/", moduleName), cloneOptions);
}

module.exports = {
    install
}