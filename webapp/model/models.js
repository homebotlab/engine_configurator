sap.ui.define(["sap/ui/model/json/JSONModel", "sap/ui/Device"], function(
  JSONModel,
  Device
) {
  "use strict";

  return {
    createDeviceModel: function() {
      var oModel = new JSONModel(Device);
      oModel.setDefaultBindingMode("OneWay");
      return oModel;
    },

    loadResourcesTypes: function(name) {
      return jQuery.ajax({
        url: "/api/v0.1/configurator/graphql",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          query: `
            {
              ${name}
            }`
        })
      });
    },

    loadObjects: function() {
      return jQuery.ajax({
        url: "/api/v0.1/configurator/graphql",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          query: `
            {
              modules {
                _id
                title
                src
                lastCommit
                description
              }
            }`
        })
      });
    },

    loadObject: function(id) {
      return jQuery.ajax({
        url: "/api/v0.1/configurator/graphql",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          query: `
            {
              module (_id: "${id}") {
                _id
                title
                src
                lastCommit
                description
                isCopied
                  grants {
                    role
                    resource
                    action
                    attributes
                  }
                }
              }`
        })
      });
    },

    getLastCommit: function() {
      return jQuery.ajax({
        url: "/api/v0.1/configurator/graphql",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          query: `{
            lastCommit 
          }
          `
        })
      });
    },

    loadModule: function({ url, username, password }) {
      return jQuery.ajax({
        url: "/api/v0.1/configurator/graphql",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          query: `
						mutation {
							loadModule (
								url: "${url || ""}", 
								username: "${username || ""}",
								password: "${password || ""}"
							)
           	}`
        })
      });
    },

    copyModule: function({ _id, newId, newPath }) {
      return jQuery.ajax({
        url: "/api/v0.1/configurator/graphql",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          query: `
						mutation {
							copyModule (_id: "${_id}", newId: "${newId}")
            }`
        })
      });
    },

    saveObject: function(oData) {
      return jQuery.ajax({
        url: "/api/v0.1/configurator/graphql",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          query: `
            mutation {
              updateModule(
                _id: "${oData._id}",
                data: {
                   title: "${oData.title}",
                   grants: [${oData.grants
                     .map(grant => {
                       return `{
                          role: "${grant.role}",
                          resource: "${grant.resource}",
                          action: "${grant.action}",
                          attributes: ${JSON.stringify(
                            grant.attributes || ""
                          )}
                     }`;
                     })
                     .join(",")}]
                }
              ) {
                _id
                title
                description
                grants {
                  role
                  resource
                  action
                  attributes
                }
              }
            }
          `
        })
      });
    }
  };
});
