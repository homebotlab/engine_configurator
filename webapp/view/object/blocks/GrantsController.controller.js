/*global location*/
sap.ui.define([
    "sap/ui/core/mvc/Controller",
    'sap/ui/model/Filter',
    'sap/ui/core/Fragment'
], function(
    Controller, Filter, Fragment
) {
    "use strict";

    return Controller.extend("engine.configurator.view.object.blocks.GrantsController", {
        /**
         * adds a new row
         */
        onAdd: function(oEvent) {
            var oModel = oEvent.getSource().getModel(),
                aSkills = oModel.getProperty("/grants") || [];
            aSkills.push({
                name: "",
                desc: ""
            });
            oModel.setProperty("/grants", aSkills);
        },

        /**
         * removes a row from table
         */
        onSkillDelete: function(oEvent) {
            var oSource = oEvent.getSource(),
                oModel = oSource.getModel(),
                aSkills = oModel.getProperty("/grants");
            aSkills.splice(aSkills.indexOf(oEvent.getParameter("listItem").getBindingContext().getObject()), 1);
            oModel.setProperty("/grants", aSkills);
        },

        handleValueHelp: function(oEvent) {
            var sInputValue = oEvent.getSource().getValue();
            var entity = oEvent.getSource().data('Entity');
            
            this._valueHelpDialog = new sap.m.SelectDialog({
                title: entity,
                //class: "sapUiPopupWithPadding",
                search: this._handleValueHelpSearch,
                confirm: this._handleValueHelpClose,
                cancel: function(){
                    this.destroy();
                }
            });

            this._valueHelpDialog.cellInput = oEvent.getSource();

            this.getView().addDependent(this._valueHelpDialog);

            this._valueHelpDialog.bindAggregation("items", {
                path: "metadata>/" + entity,
                template: new sap.m.StandardListItem({
                    title: "{metadata>name}"
                })
            })

            //this.inputId = oEvent.getSource().getId();
            // create value help dialog
            // if (!this._valueHelpDialog) {
            //     this._valueHelpDialog = sap.ui.xmlfragment(
            //         "engine.configurator.view.object.blocks.ActionTypeDialog",
            //         this
            //     );
            //     this.getView().addDependent(this._valueHelpDialog);
            // }

            // create a filter for the binding
            // this._valueHelpDialog.getBinding("items").filter([new Filter(
            //     "name",
            //     sap.ui.model.FilterOperator.Contains, sInputValue
            // )]);

            // open value help dialog filtered by the input value
            this._valueHelpDialog.open(sInputValue);
        },

        _handleValueHelpSearch: function(evt) {
            var sValue = evt.getParameter("value");
            var oFilter = new Filter(
                "name",
                sap.ui.model.FilterOperator.Contains, sValue
            );
            evt.getSource().getBinding("items").filter([oFilter]);
        },

        _handleValueHelpClose: function(evt) {
            var oSelectedItem = evt.getParameter("selectedItem");
            if (oSelectedItem) {
                this.cellInput.setValue(oSelectedItem.getTitle());
            }
            evt.getSource().getBinding("items").filter([]);
            this.destroy();
        }

    });
});