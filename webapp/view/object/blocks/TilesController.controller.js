/*global location*/
sap.ui.define([
    "sap/ui/core/mvc/Controller"
], function(
    Controller
) {
    "use strict";

    return Controller.extend("engine.configurator.view.object.blocks.TilesController", {
      /**
       * adds a new row
       */
      onAdd: function (oEvent) {
        var oModel = oEvent.getSource().getModel(),
          aSkills = oModel.getProperty("/tiles") || [];
        aSkills.push({
          name: "",
          desc: ""
        });
        oModel.setProperty("/tiles", aSkills);
      },

      /**
       * removes a row from table
       */
      onDelete: function (oEvent) {
        var oSource = oEvent.getSource(),
          oModel = oSource.getModel(),
          aSkills = oModel.getProperty("/tiles");
        aSkills.splice(aSkills.indexOf(oEvent.getParameter("listItem").getBindingContext().getObject()), 1);
        oModel.setProperty("/tiles", aSkills);
      }

    });
});