sap.ui.define(['sap/uxap/BlockBase'], function (BlockBase) {
	"use strict";

	var GrantsBlock = BlockBase.extend("engine.configurator.view.object.blocks.Grants", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "engine.configurator.view.object.blocks.Grants",
					type: "XML"
				},
				Expanded: {
					viewName: "engine.configurator.view.object.blocks.Grants",
					type: "XML"
				}
			}
		}
	});
	return GrantsBlock;
});