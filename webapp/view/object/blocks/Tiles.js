sap.ui.define(['sap/uxap/BlockBase'], function (BlockBase) {
	"use strict";

	var TilesBlock = BlockBase.extend("engine.configurator.view.object.blocks.Tiles", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "engine.configurator.view.object.blocks.Tiles",
					type: "XML"
				},
				Expanded: {
					viewName: "engine.configurator.view.object.blocks.Tiles",
					type: "XML"
				}
			}
		}
	});
	return TilesBlock;
});