const bodyParser = require('body-parser');
const { Router } = require('express');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');

const schema = require('../graphql');

const router  = Router();

router.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));
router.use('/graphiql', graphiqlExpress({ endpointURL: '/api/v0.1/configurator/graphql' }));

module.exports = router;